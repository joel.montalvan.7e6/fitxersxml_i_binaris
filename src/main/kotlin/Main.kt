import kotlinx.serialization.BinaryFormat
import java.io.*
import kotlinx.serialization.Serializable
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import nl.adaptivity.xmlutil.serialization.XML
import nl.adaptivity.xmlutil.xmlEncode
import java.io.File
import java.net.ProtocolException
import kotlin.io.path.*

@Serializable
data class Grade(
    val date: String,
    val mark: String,
    val score: Int
): java.io.Serializable

@Serializable
data class Address(
    val building: String,
    val coord: List<Double>,
    val street: String,
    val zipcode: String
): java.io.Serializable

@Serializable
data class Restaurant(
    val address: Address,
    val borough: String,
    val cuisine: String,
    val grades: List<Grade>,
    val name: String,
    val restaurant_id: String
): java.io.Serializable



fun main(){
    //ejercicio2()
    //ejercico3()
    //ejercicio4()
    ejercicio5()

}

fun ejercicio2(){
    val lineasJson = File ("/dades/JOELMONTALVAN/M06/fitxersBinaris_i_xml/src/Fitxers/restaurants.json").readLines()
    val ficherBinari = File("/dades/JOELMONTALVAN/M06/fitxersBinaris_i_xml/src/Fitxers/restaurants.dat")
    val binari = ObjectOutputStream(FileOutputStream(ficherBinari))
    for (linea in lineasJson){
        val valueRestaurant = Json.decodeFromString<Restaurant>(linea)
        binari.writeObject(valueRestaurant)
    }
    binari.close()

}

fun ejercico3(){
    val input = ObjectInputStream(FileInputStream("/dades/JOELMONTALVAN/M06/fitxersBinaris_i_xml/src/Fitxers/restaurants.dat"))
    while (true) {
        try {
            val restaurant: Restaurant = input.readObject() as Restaurant
            val xmlRestaurant= XML.encodeToString(restaurant)
            Path("/dades/JOELMONTALVAN/M06/fitxersBinaris_i_xml/src/Fitxers/restaurants.xml").appendText("$xmlRestaurant\n")
        } catch (e: EOFException) {
            break
        }
    }
    input.close()
}

fun ejercicio4(){
    val lineasXml = Path("/dades/JOELMONTALVAN/M06/fitxersBinaris_i_xml/src/Fitxers/restaurants.xml").readLines()
    for(linea in lineasXml){
        val restaurant : Restaurant = XML.decodeFromString(linea)
        println(restaurant.name)
    }
}

fun ejercicio5(){
    {"address": {"building": "2780", "coord": [-73.98241999999999, 40.579505],
        "street": "Stillwell Avenue", "zipcode": "11224"},
        "borough": "Brooklyn", "cuisine": "American ",
        "grades": [{"date":"1990-05-01", "mark": "A", "score": 5},
        {"date":"1989-05-01", "mark": "A", "score": 7},
        {"date":"1988-05-01", "mark": "A", "score": 12},
        {"date":"1987-05-01", "mark": "A", "score": 12}],
        "name": "Riviera Caterer", "restaurant_id": "40356018"}
    val xmlRestaurant= Restaurant(Address("2781", listOf(80.234567, 50.475), "Av. Vall de Hebron", "08032"), "El Carmelo", "Spanish", )
    Path("/dades/JOELMONTALVAN/M06/fitxersBinaris_i_xml/src/Fitxers/restaurants.xml").appendText("$xmlRestaurant\n")
}



